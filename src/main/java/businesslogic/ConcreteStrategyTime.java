package businesslogic;

import model.Server;
import model.Task;

import java.util.List;

public class ConcreteStrategyTime implements Strategy{
    public ConcreteStrategyTime() {
    }

    public void addTask(List<Server> servers, Task t){
        if(!servers.isEmpty()) {
        Server serverToAdd = servers.get(0);
        int shortestPer=serverToAdd.getWaitingPeriod().get();
        for(Server server : servers){
            int waitingPer=server.getWaitingPeriod().get();
            if(shortestPer>waitingPer){
                shortestPer=waitingPer;
                serverToAdd=server;
            }
        }

        serverToAdd.addTask(t);
    }
    }



}
