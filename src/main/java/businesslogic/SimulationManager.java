package businesslogic;

import model.Server;
import model.SimulationFrame;
import model.Task;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

import static java.lang.Thread.sleep;


public class SimulationManager implements Runnable {

    public int timeLimit ;//=100;
    public int maxProcessingTime;//=10;
    public int minProcessingTime;//=2;
    public int numberOfServers;//=3;
    public int numberOfClients;//=100;
    public int minServiceTime;
    public int maxServiceTime;
    public SelectionPolicy selectionPolicy = SelectionPolicy.SHORTEST_QUEUE;

    public double totalWaitingTime = 0.0;
    public double totalTasksServed = 0.0;
    public double averageWaitingTime;



    private Scheduler scheduler;
    private SimulationFrame frame;
    private List<Task> generatedTasks=new ArrayList<>();

    public void setGeneratedTasks(List<Task> generatedTasks) {
        this.generatedTasks = generatedTasks;
    }

    public List<Task> getGeneratedTasks() {
        return generatedTasks;
    }

    public void setScheduler(Scheduler scheduler) {
        this.scheduler = scheduler;
    }

    public Scheduler getScheduler() {
        return scheduler;
    }

    public SimulationManager(){

        this.scheduler = new Scheduler(numberOfServers, numberOfClients);
        for(int i=0;i<numberOfServers;i++){

            Server server1=new Server();
            Thread Thread1=new Thread(server1);
            Thread1.start();
        }

        generateNRandomTasks(numberOfClients,maxProcessingTime,minProcessingTime , minServiceTime,maxServiceTime);

    }
    private void generateNRandomTasks(int n , int maxProcessingTime , int minProcessingTime , int minServiceTime , int maxServiceTime){

        Random random = new Random();
        for (int i = 1; i <=n; i++) {
            int arrivalTime = minProcessingTime+random.nextInt(maxProcessingTime - minProcessingTime);
            int processingTime=minServiceTime+random.nextInt(maxServiceTime - minServiceTime) ;
            Task task = new Task(i, arrivalTime, processingTime);
            generatedTasks.add(task);


        }
        generatedTasks.sort(Comparator.comparingInt(Task::getArrivalTime));


    }


    @Override
    public void run() {
        File outputFile = new File("output.txt");
        try (PrintWriter writer = new PrintWriter(new FileWriter(outputFile))) {
            int currentTime = 0;
            while (currentTime <= timeLimit) {

                List<Task> currentWaitingTasks = new ArrayList<>();
                Iterator<Task> iterator = generatedTasks.iterator();
                writer.println("Time " + currentTime);
                writer.print("Waiting clients: "); writer.println();
                while (iterator.hasNext()) {Task task = iterator.next();
                    if (task.getArrivalTime() > currentTime) {currentWaitingTasks.add(task);}
                    else if (task.getArrivalTime() == currentTime) {scheduler.dispatchTask(task);iterator.remove();}
                }
                for (Task t : currentWaitingTasks) {
                    writer.print("(" + t.getID() + "," + t.getArrivalTime() + "," + t.getServiceTime() + ") ");}writer.println();List<Server> servers = scheduler.getServers();
                for (int i = 0; i < servers.size(); i++) {
                    writer.print("Queue " + (i + 1) + ": ");
                    Task[] tasks = servers.get(i).getTasks();
                    if (tasks.length == 0) {
                        writer.println("closed");} else {for (Task task1 : tasks) {
                            this.totalWaitingTime += task1.getWaitingTime();
                            this.totalTasksServed++;
                            writer.print("(" + task1.getID() + "," + task1.getArrivalTime() + "," + task1.getServiceTime() + ") ");}writer.println();}}
                writer.println();
                currentTime++;
                try {
                    sleep(1000);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    break;
                    }
                this.averageWaitingTime = this.totalWaitingTime / this.totalTasksServed;}
            writer.println("Average waiting time: " + this.averageWaitingTime);

        } catch (IOException e) {
            System.err.println("Error writing to file: " + e.getMessage());}}

    public static void main(String[] args){

        SimulationManager m1 = new SimulationManager();

        m1.numberOfClients=10;
        m1.numberOfServers=2;
        m1.timeLimit=30;
        m1.maxProcessingTime=10;
        m1.minProcessingTime=2;
        m1.minServiceTime=2;
        m1.maxServiceTime=4;

        m1.generateNRandomTasks(m1.numberOfClients,m1.maxProcessingTime,m1.minProcessingTime,m1.minServiceTime,m1.maxServiceTime);
        m1.setGeneratedTasks(m1.generatedTasks);
        //m1.selectionPolicy= SelectionPolicy.SHORTEST_QUEUE;
        //m1.getScheduler().changeStrategy(SelectionPolicy.SHORTEST_QUEUE);
        Thread simulationThread = new Thread(m1);

        simulationThread.start();



    }

}
