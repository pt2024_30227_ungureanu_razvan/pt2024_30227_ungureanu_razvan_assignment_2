package businesslogic;

import model.Server;
import model.Task;

import java.util.List;

public class ConcreteStrategyQeue implements Strategy{


    public void addTask(List<Server> servers, Task t){
        if(!servers.isEmpty()) {
            Server serverToAdd = servers.get(0);
            int shortestQeue=serverToAdd.getTasks().length;
            for(Server server : servers){
                int sQeue=server.getTasks().length;
                if(sQeue>shortestQeue){
                    shortestQeue=sQeue;
                    serverToAdd=server;
                }
            }

            serverToAdd.addTask(t);
        }
    }


}
