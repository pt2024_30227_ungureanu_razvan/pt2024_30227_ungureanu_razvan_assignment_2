package businesslogic;

import model.Server;
import model.Task;

import java.util.ArrayList;
import java.util.List;
public class Scheduler {

    private List<Server>servers;
    private int maxNoServers;
    private int maxTasksPerServer;
    private Strategy strategy;

    public Scheduler(int maxNoServers, int maxTasksPerServer) {
        this.servers = new ArrayList<>();
        this.maxNoServers = maxNoServers;
        this.maxTasksPerServer = maxTasksPerServer;
        strategy = new ConcreteStrategyTime();
        for(int i=0;i<maxNoServers;i++){
            Server server=new Server();
            servers.add(server);

            Thread thread = new Thread(() -> server.run());

            thread.start();
        }
    }

    public void setStrategy(Strategy strategy) {
        this.strategy = strategy;
    }

    public void changeStrategy(SelectionPolicy policy){

        if(policy==SelectionPolicy.SHORTEST_QUEUE){
            strategy=new ConcreteStrategyQeue();
        }
        else  if(policy==SelectionPolicy.SHORTEST_TIME){
            strategy=new ConcreteStrategyTime();
        }
    }

    public void dispatchTask(Task t){
        strategy.addTask(servers,t);
    }

    public List <Server> getServers(){
        return servers;
    }

    @Override
    public String toString() {

        return null;
    }
}
