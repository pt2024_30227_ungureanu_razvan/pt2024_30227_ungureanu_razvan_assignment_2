package model;

public class Task {
    private int ID;
    private int arrivalTime;
    private int serviceTime;
    private int waitingTime;

    public Task() {
        this.ID = 0;
        this.arrivalTime = 0;
        this.serviceTime = 0;
        this.waitingTime=0;
    }

    public Task(int ID, int arrivalTime, int serviceTime) {
        this.ID = ID;
        this.arrivalTime = arrivalTime;
        this.serviceTime = serviceTime;

    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getServiceTime() {
        return serviceTime;
    }

    public int getWaitingTime() {
        return waitingTime;
    }

    public void setWaitingTime(int waitingTime) {
        this.waitingTime = waitingTime;
    }

    public void setArrivalTime(int arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public void setServiceTime(int serviceTime) {
        this.serviceTime = serviceTime;
    }

    public int getID() {
        return ID;
    }

    public int getArrivalTime() {
        return arrivalTime;
    }

    public int getserviceTime() {
        return serviceTime;
    }
}
