package model;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.atomic.AtomicInteger;

public class Server implements Runnable {
    private BlockingQueue<Task>tasks;
    private AtomicInteger  waitingPeriod;

    public Server() {
        this.tasks = new LinkedBlockingDeque<>();
        this.waitingPeriod =new AtomicInteger(0);

    }

    public void addTask(Task newTask){

        this.tasks.add(newTask);
        this.waitingPeriod.addAndGet(newTask.getserviceTime());
        newTask.setWaitingTime(this.waitingPeriod.get());

    }
    public void run() {
        while (true) {
            try {
                Task t1 = tasks.peek(); //ma uit la primul element din coada fara sa l scot
                if (t1 != null) {
                    Thread.sleep(t1.getserviceTime() * 1000);
                    tasks.poll(); //daca i-a trecut service time-ul scot clientul
                    waitingPeriod.addAndGet(-t1.getserviceTime());
                }
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                break;
            }
        }
    }

     public Task[] getTasks(){

         return tasks.toArray(new Task[0]);
    }

    public AtomicInteger getWaitingPeriod() {
        return waitingPeriod;
    }
}
